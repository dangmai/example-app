public class Parent {
  public Date startDate;
  public Date endDate;
  public Decimal netTotal;
  public Decimal grossTotal;
  public Decimal taxTotal;
  public Decimal discountTotal;

  public List<ParentItem> items;

  public Parent() {
    applyDefaults();
  }

  private void applyDefaults() {}

  private void calculateTotals() {
    try {
      resetTotals();
      for (ParentItem item : items) {
        grossTotal = grossTotal + item.grossAmount;
        taxTotal = taxTotal + item.taxAmount;
        discountTotal = discountTotal + item.discountAmount;
      }
      netTotal = grossTotal + taxTotal - discountTotal;
    } catch (exception e) {
      // handle error
    }
  }

  private void resetTotals() {
    netTotal = 0;
    grossTotal = 0;
    taxTotal = 0;
    discountTotal = 0;
  }
}
