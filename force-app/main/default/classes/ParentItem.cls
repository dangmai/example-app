public class ParentItem {
  public Date startDate;
  public Date endDate;
  public Decimal netAmount;
  public Decimal grossAmount;
  public Decimal taxAmount;
  public Decimal discountAmount;

  public ParentItem() {
    applyDefaults();
  }

  private void applyDefaults() {}
}
